# API Doc

## Actors

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /actors   | GET |  | 200: ``` [ { "id": 42, "name": "John", "surname": "Travolta"}, {..} ] ``` |
| /actors| POST      |   | 201: ``` { "id": 42, "name": "John", "surname": "Travolta", "movies": [ {"id": 1, "title": "Titre du film"}]} ``` |
| /actors/{id}   | GET      |     | 200: ``` { "id": 42, "name": "John", "surname": "Travolta", "movies": [ {"id": 1, "title": "Titre du film"}]} ``` |

## Movie

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /movies | GET |  | 200: ``` [ { "id": 42, "title": "Titre", "year": "Année...", "poster": "poster", "synopsis": "synopsis", category: { "id": 42, "name": "Nom de la categorie"}, actor: {"id": "42", "name": "John", "surname": "Travolta" {..} ] ``` |
| /movies    | POST      |    | 201: ``` { "id": 42, "title": "Titre", "year": "Année...", category: { "id": 42, "name": "Nom de la categorie"}, actor: {"id": "42", "name": "John", "surname": "Travolta" }``` | 200: ``` { "id": 42, "title": "Titre", "year": "Année...", "poster": "poster", "synopsis": "synopsis", category: { "id": 42, "name": "Nom de la categorie"}, actor: {"id": "42", "name": "John", "surname": "Travolta" }``` |
| /movies /{id}   | GET      |     | 200: ``` { "id": 42, "title": "Titre", "year": "Année...", "poster": "poster", "synopsis": "synopsis", category: { "id": 42, "name": "Nom de la categorie"}, actor: {"id": "42", "name": "John", "surname": "Travolta" }``` |
| /movies /{id}   | DELETE      |     | 200 ``` {} ``` |
| /movies /{id}   | PUT      |  | 200: ``` { "id": 42, "title": "Titre", "year": "Année...", "category": "42", "actor": "42" }``` |


## Category

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
|/categories | GET |  | 200: ``` [ { "id": 42, "name": "Nom" } ] ``` |
|/categories / {id} | GET |  | 200: ``` [ { "id": 42, "name": "Nom" } ] ``` |
| /categories   | POST      |    | 201: ``` { "id": 42, "name": "Nom"}``` |
| /categories/{id}   | GET      |     | 200: ``` { "id": 42, "name": "Nom"}``` |
| /categories/{id}   | DELETE      |     | 200 ``` {} ``` |
| /categories/{id}   | PUT      |     | 200: ``` { "id": 42, "name": "Nom"}``` |