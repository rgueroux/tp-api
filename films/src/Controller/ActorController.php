<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ActorController extends AbstractController
{
    function getAllActors() {
        $actors= [
            ["id" => '1', "name" => "Nom 1", "surname" => "Prenom 1"],
            ["id" => '2', "name" => "Nom 2", "surname" => "Prenom 2"],
            ["id" => '3', "name" => "Nom 3", "surname" => "Prenom 3"],
        ];
        $jsonResponse = json_encode($actors);

        // return new Response($jsonResponse);
        return $this->render('actors.html.twig', [
            'actors' => $actors,
        ]);
    }

    function getActor($id) {
        $actor= [
            "id" => $id,
            "name" => "Nom ",
            "surname" => "Prenom"
        ];

        $jsonResponse = json_encode($actor);

        return $this->render('actor.html.twig', [
            'actor' => $actor,
        ]);
    }

    function createActor(Request $request){
      //récuperation de la chaine de caractere saisi par l'utilisateur
      $bodyRequest = $request -> getContent();
      $bodyRequest = json_decode($bodyRequest);

      //traitement de la création
      $jsonResponse = json_encode($bodyRequest);

      return new Response($jsonResponse);
    }
}
