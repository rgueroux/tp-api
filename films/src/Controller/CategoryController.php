<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    function getAllCategories() {
        $categories= [
            ["id" => '1', "name" => "Categorie 1"],
            ["id" => '2', "name" => "Categorie 2"],
            ["id" => '3', "name" => "Categorie 3"],
        ];
        $jsonResponse = json_encode($categories);

        // return new Response($jsonResponse);
        return $this->render('categories.html.twig', [
            'categories' => $categories,
        ]);
    }

    function getCategory($id) {
        $category= [
            "id" => $id,
            "name" => "Categorie ".$id
        ];

        $jsonResponse = json_encode($category);

        return $this->render('category.html.twig', [
            'category' => $category,
        ]);
    }

    function deleteCategory($id){
        $jsonResponse = json_encode([]);
        return new Response($jsonResponse);
    }

    function createCategory(Request $request) {

      //récuperation de la chaine de caractere saisi par l'utilisateur
      $bodyRequest = $request -> getContent();
      $bodyRequest = json_decode($bodyRequest);

      //traitement de la création
      $jsonResponse = json_encode($bodyRequest);

      return new Response($jsonResponse);
    }
}
