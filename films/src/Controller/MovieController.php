<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MovieController extends AbstractController
{
    function getAllMovies() {
        $movies= [
            ["id" => '1',"titre" => "Film 1", "année" => "année1", "poster" => "poster1", "synopsis" => "synopsis1" ],
            ["id" => '2',"titre" => "Film 2", "année" => "année2", "poster" => "poster2", "synopsis" => "synopsis2" ],
            ["id" => '3',"titre" => "Film 3", "année" => "année3", "poster" => "poster3", "synopsis" => "synopsis3" ],
        ];
        $jsonResponse = json_encode($movies);

        return $this->render('movies.html.twig', [
            'movies' => $movies,
        ]);

    }

    function getMovie($id) {
        $movie= [
            "id" => $id,
            "titre" => "Film ".$id,
            "année" => "année".$id,
            "poster" => "poster".$id,
            "synopsis" => "synospis".$id
        ];

        $jsonResponse = json_encode($movie);

		return $this->render('movie.html.twig', [
            'movie' => $movie,
        ]);        
    }

    function createMovie(Request $request) {
    	
        $body = $request->getContent();

        $body = json_decode($body);

        $responseJson = json_encode($body);

        return new Response($responseJson);
    }

    function deleteMovies($id){
        $jsonResponse = json_encode([]);
        return new Response($jsonResponse);
    }

    function updateMovies($id) {
        $movie= [
            "id" => $id,
            "titre" => "Film ".$id,
            "année" => "année".$id,
            "poster" => "poster".$id,
            "synopsis" => "synopsis".$id
        ];

        $jsonResponse = json_encode($movie);

        return new Response($jsonResponse);
    }
}
